package com.firstfewlines;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootH2Example {
    public static void main(String [] argv){
        SpringApplication.run(SpringBootH2Example.class, argv);
    }
}
